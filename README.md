# Haiku GPU

drm3 drivers for the Haiku operating system.

**THIS REPO MEANS NOTHING**: Don't think this is news worthy.
It may not go anywhere. It's mostly an experiment in building
DRM drivers out of tree.

## Build Requirements

* copy of linux source tree
* scons

## Building

```
export LINUX_SRC=/src/linux
scons
```

## Cleaning up
```
scons -c
```
